@echo off
setlocal EnableExtensions EnableDelayedExpansion

rem Fix_hdc v0.02
rem created by cdob
rem based on fixide by Markus Debus www.pecd.net
rem based on http://support.microsoft.com/kb/314082/

rem Requirements: 
rem
rem XP/2003 with MassStorage drivers integrated to txtsetup.sif
rem use e.g. http://www.driverpacks.net/
rem
rem external applications: reg.exe regedit.exe findstr.exe expand.exe
rem decvon.exe - http://support.microsoft.com/kb/311272/

set Source_SystemRoot=%SystemRoot%

rem parse command line
set make=
:begin_parse
 if /I [%~1]==[-M] set make=MassStorage
 if /I [%~1]==[-R] set make=Restore
 if /I [%~1]==[-T] (set target_root=%~2&shift)
 if /I [%~1]==[-S] (set Source_SystemRoot=%~2&shift)
 if /I [%~1]==[-H] (call :Help &goto :eof)
 if    [%~1]==[-?] (call :Help &goto :eof)
 shift
 if [%1]==[] goto :exit_parse
goto begin_parse
:exit_parse

if not DEFINED target_root goto :Ask
if DEFINED make goto %make%

:Ask
title fix_hdc  %target_root%
cls& ECHO. &ECHO  fix_hdc:  Fix for some Stop 0x0000007B Errors
ECHO  based on http://support.microsoft.com/kb/314082/
ECHO  Remember other hardware differences can cause stop 0x7b too.
ECHO. &ECHO  First backup data yourself. &ECHO  This comes with ABSOLUTELY NO WARRANTY.
ECHO. &ECHO  TargetRoot: %target_root% &ECHO.
if exist %target_root%\system32\config\SYSTEM.fix_hdc ECHO  Backup exists and can be restored.
if not exist %target_root%\system32\config\SYSTEM.fix_hdc ECHO  Backup does not exist and will be created.
ECHO. &ECHO  (M) Update MassStorage drivers
ECHO  (T) Set TargetRoot
if exist %target_root%\system32\config\SYSTEM.fix_hdc ECHO  (R) Restore Backup
ECHO  (0) Do nothing and exit &ECHO.

set /p _task= Your choice ? 
if /I "%_task%"=="U" goto MassStorage
if /I "%_task%"=="M" goto MassStorage
if /I "%_task%"=="R" goto Restore
if /I "%_task%"=="T" (call :read_target_root & goto Ask)
if "%_task%"=="0" goto :eof
Echo  No selection... &pause &goto :eof


:Restore ==============================================================
call :verify_target_root
echo restoring %target_root%\system32\config\SYSTEM
if exist %target_root%\system32\config\SYSTEM.fix_hdc move %target_root%\system32\config\SYSTEM.fix_hdc %target_root%\system32\config\SYSTEM
goto :eof

:MassStorage ==========================================================
call :verify_target_root
if not exist %target_root%\system32\config\SYSTEM.fix_hdc ( 
echo creating backup of %target_root%\system32\config\SYSTEM
copy %target_root%\system32\config\SYSTEM %target_root%\system32\config\SYSTEM.fix_hdc
)
set hdc_logfile=%target_root%\system32\config\fix_hdc.log
echo. &echo Create MassStorage HardwareID log file %hdc_logfile%
(echo %date% %time% &echo. &echo devcon hwids PCI\CC_01* &echo.)>"%hdc_logfile%"
devcon.exe hwids PCI\CC_01* >>"%hdc_logfile%"

reg.exe UNLOAD HKLM\SYSTEM_00 >nul 2>&1
reg.exe LOAD HKLM\SYSTEM_00 %target_root%\system32\config\SYSTEM >nul 2>&1

rem detect CurrentControlSet
for /f "tokens=3" %%a in ('reg.exe query "HKLM\SYSTEM_00\Select" /v "Current"') do set /a ControlSet=%%a
set ControlSet=00000%ControlSet%
set ControlSet=ControlSet%ControlSet:~-3%
echo. &echo ControlSet "%ControlSet%" used.

echo REGEDIT4> "%temp%\fix_hdc.reg"
set HardwareIDFound=

::=== Mass Storage Device =============================================
::Base Type = 1: Mass Storage Device
::  Sub-Type =  0 : SCSI Controller
::  Sub-Type =  1 : IDE Controller (Standard ATA compatible)
::  Sub-Type =  2 : Floppy Controller (Standard 765 compatible)
::  Sub-Type =  3 : IPI Controller
::  Sub-Type =  4 : RAID Controller
::  Sub-Type =  6 : AHCI Controller
::  Sub-Type = 80h: Other Mass Storage Controller

::=== Mass Storage Device - non IDE ===================================
for %%a in (CC_0100 CC_0103 CC_0104 CC_0106 CC_0180) do call :insert_driver %%a

::=== Mass Storage Device - IDE controller ============================
set HardWareID=
for /f "tokens=1-2* delims=\& " %%a IN ('devcon.exe hwids PCI\CC_0101') DO (
  if "%%a"=="PCI" set HardWareID=%%a\%%b)
if "%HardWareID%"=="PCI\CC_0101" (
  echo. &echo. &echo IDE controller found. Add additional files.
  rem don't replace a existand default driver
  for %%a in (Atapi Intelide Pciide Pciidex) do if not exist %target_root%\system32\drivers\%%a.sys (
    for %%t in (%Source_SystemRoot%\%%a.sy? %Source_SystemRoot%\system32\drivers\%%a.sy?) do (
      expand.exe %%t %target_root%\system32\drivers\%%a.sys)
   )

  call :Add_CriticalDeviceDatabase 4D36E96A atapi primary_ide_channel
  call :Add_CriticalDeviceDatabase 4D36E96A atapi secondary_ide_channel
  call :Add_CriticalDeviceDatabase 4D36E96A atapi *pnp0600
  call :Add_CriticalDeviceDatabase 4D36E96A atapi *azt0502
  call :Add_CriticalDeviceDatabase 4D36E967 disk gendisk
  call :Add_CriticalDeviceDatabase 4D36E96A pciide pci#cc_0101
  call :Add_CriticalDeviceDatabase 4D36E96A intelide pci#ven_8086 cc_0101

  (echo.
   echo [HKEY_LOCAL_MACHINE\SYSTEM_00\%ControlSet%\Services\atapi]
   echo "ErrorControl"=dword:00000001
   echo "Group"="SCSI miniport"
   echo "Start"=dword:00000000
   echo "Tag"=dword:00000019
   echo "Type"=dword:00000001
   echo "DisplayName"="Standard IDE/ESDI Hard Disk Controller"
   echo "ImagePath"="system32\\drivers\\atapi.sys"
   echo.
   echo [HKEY_LOCAL_MACHINE\SYSTEM_00\%ControlSet%\Services\IntelIde]
   echo "ErrorControl"=dword:00000001
   echo "Group"="System Bus Extender"
   echo "Start"=dword:00000000
   echo "Tag"=dword:00000004
   echo "Type"=dword:00000001
   echo "ImagePath"="system32\\drivers\\intelide.sys"
   echo.
   echo [HKEY_LOCAL_MACHINE\SYSTEM_00\%ControlSet%\Services\PCIIde]
   echo "ErrorControl"=dword:00000001
   echo "Group"="System Bus Extender"
   echo "Start"=dword:00000000
   echo "Tag"=dword:00000003
   echo "Type"=dword:00000001
   echo "ImagePath"="system32\\drivers\\pciide.sys"
  )>>"%temp%\fix_hdc.reg"
)

::=== finish ==========================================================
echo. &echo Importing registry file "%temp%\fix_hdc.reg"
regedit.exe /S "%temp%\fix_hdc.reg"
reg.exe UNLOAD HKLM\SYSTEM_00 >nul 2>&1
(echo. &echo. &echo Importing %temp%\fix_hdc.reg: &type "%temp%\fix_hdc.reg") >>"%hdc_logfile%"

echo. &echo Fix_hdc finished. Good luck.
echo. &pause &title DOS
goto :eof === END =====================================================

::=====================================================================
:insert_driver
for /f "tokens=1-2* skip=1 delims=\& " %%a IN ('devcon.exe hwids PCI\%1') DO (
  if "%%a"=="Name:" (echo. &echo %%a %%b %%c &set i_count=0 &set HardwareIDFound=)
  set /a i_count+=1
  if !i_count! EQU 3 echo Search for "%%a\%%b&%%c"
  if not DEFINED HardwareIDFound if "%%a"=="PCI" if not "%%c"=="" (
    set HardWareID="%%a\%%b&%%c"
    rem echo HardWareID !HardWareID!

    rem exclude PCI\VEN_nnnn
    set bb=%%b
    if "%%c"=="" if "!bb:~0,3!"=="VEN" set HardWareID=

    if DEFINED HardwareID for /f "tokens=1-2* delims==" %%h IN ('findstr.exe /I !HardWareID! "%Source_SystemRoot%\txtsetup.sif"') DO (
      set HardwareIDFound=%%h %%~i
      set driver=%%~i
    )
    
    set cc=%%c
    if not DEFINED HardwareIDFound if "!cc:~0,3!"=="CC_" if "!cc:~7,1!"=="" echo Warning: driver not found
     
    if DEFINED HardwareIDFound (
      echo HardwareID found: !HardwareIDFound!  
      rem Replace drivers, even if previous exist. Maybe a old version.
      echo. &echo copy driver: !driver!
      for %%t in (%Source_SystemRoot%\!driver!.sy? %Source_SystemRoot%\system32\drivers\!driver!.sy?) do (
       expand.exe %%t %target_root%\system32\drivers\!driver!.sys)

      (echo. &echo Add driver !HardWareID! !driver! 
       echo to disable driver: sc config !driver! start= disabled)>>"%hdc_logfile%"

      set HardWareID=%%a#%%b^&%%c
      set ClassGUID={4D36E97B-E325-11CE-BFC1-08002BE10318}
      if "%1"=="CC_0106" set ClassGUID={4D36E96A-E325-11CE-BFC1-08002BE10318}
      
      rem driver: http://support.microsoft.com/kb/103000/
      echo. &echo Adding !HardWareID! to fix_hdc.reg
      (echo.
      echo [HKEY_LOCAL_MACHINE\SYSTEM_00\%ControlSet%\Control\CriticalDeviceDatabase\!HardWareID!]
      echo "ClassGUID"="!ClassGUID!"
      echo "Service"="!driver!"
      echo.
      echo [HKEY_LOCAL_MACHINE\SYSTEM_00\%ControlSet%\Services\!driver!]
      echo "ErrorControl"=dword:00000001
      echo "Group"="SCSI miniport"
      echo "Start"=dword:00000000
      echo "Type"=dword:00000001
      echo "ImagePath"="system32\\drivers\\!driver!.sys")>>"%temp%\fix_hdc.reg"
   )
  )
)
goto :eof =============================================================

::=====================================================================
:Add_CriticalDeviceDatabase
echo.>>"%temp%\fix_hdc.reg"
if "%~4"=="" (
  echo [HKEY_LOCAL_MACHINE\SYSTEM_00\%ControlSet%\Control\CriticalDeviceDatabase\%~3]>>"%temp%\fix_hdc.reg"
) else (
  echo [HKEY_LOCAL_MACHINE\SYSTEM_00\%ControlSet%\Control\CriticalDeviceDatabase\%~3^&%~4]>>"%temp%\fix_hdc.reg"
)
(echo "ClassGUID"="{%~1-E325-11CE-BFC1-08002BE10318}"
echo "Service"="%~2")>>"%temp%\fix_hdc.reg"
goto :eof =============================================================

::=====================================================================
:read_target_root
set /p target_root= Enter target_root: 
goto :eof =============================================================

::=====================================================================
:verify_target_root
if "%target_root%"=="" (
  call :Help Error: target_root not set &exit)
if not exist "%target_root%" (
  call :Help Error: target_root "%target_root%" not found &exit)
if not exist "%target_root%\system32\config\SYSTEM" (
  call :Help Error: file "%target_root%\system32\config\SYSTEM" not found &exit)
if not exist "%Source_SystemRoot%\txtsetup.sif" ( 
  call :Help Error: %Source_SystemRoot%\txtsetup.sif not found &exit)
goto :eof =============================================================

::=====================================================================
:Help
ECHO. &ECHO  %~nx0:  Fix for some Stop 0x0000007B Errors
ECHO  based on http://support.microsoft.com/kb/314082/ &echo.
echo  -M : MassStorage mode
echo  -R : Restore mode
echo  -s : source folder to txtsetup.sif (BartPE or windows installation folder)
echo  -t : target_root (for example c:\windows)
echo.
if not "%1"=="" (echo %* &echo.)
pause
goto :eof =============================================================