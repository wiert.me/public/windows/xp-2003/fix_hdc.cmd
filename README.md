# FixHDC

Hard to find [UBCD4WIN](http://www.ubcd4win.com/) source for `fix_hdc.cmd` v0.02 by cdob, also part of the ["Old Versions"](https://www.hirensbootcd.org/old-versions/) of [Hiren's Boot CD](https://www.hirensbootcd.org/)

Depends on Windows XP/2003 built-in tools [`reg.exe`](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/reg), `regedit.exe`[`findstr.exe`](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/findstr), [`expand.exe`](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/expand), and the [`decvon.exe`]() from [support.microsoft.com/kb/311272](http://support.microsoft.com/kb/311272)

The `fix_hdc` subdirectory was copied from `C:\UBCD4Win\plugin\Registry\fix_hdc` after installing UBCD4WIN 3.60.
